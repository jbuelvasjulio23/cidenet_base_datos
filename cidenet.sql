-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 07-04-2021 a las 02:55:01
-- Versión del servidor: 10.4.11-MariaDB
-- Versión de PHP: 7.4.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `cidenet`
--
CREATE DATABASE IF NOT EXISTS `cidenet` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `cidenet`;

DELIMITER $$
--
-- Procedimientos
--
DROP PROCEDURE IF EXISTS `editar_empleado`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `editar_empleado` (IN `idd` VARCHAR(20), IN `p_apell` VARCHAR(20), IN `s_apell` VARCHAR(20), IN `p_nomb` VARCHAR(20), IN `s_nomb` VARCHAR(20), IN `t_document` VARCHAR(25), IN `n_document` VARCHAR(20), IN `f_ingres` VARCHAR(11), IN `area1` VARCHAR(25), IN `paiss` VARCHAR(20), IN `email` VARCHAR(50))  BEGIN
	declare canti1 int;
    declare dominio varchar(20);
    declare f_registro varchar(20);
    declare resultado varchar(50);
    declare editemail boolean;
    
    set resultado = email;
    set editemail = false;
    
		case paiss 
			when "Colombia" then
				set dominio = "cidenet.com.co";
			when "Estados unidos" then
				set dominio = "cidenet.com.us";
			else 
				set dominio = "";
		end case;
        
        select count(*) into canti1 from empleado where id=idd and p_nombre = p_nomb;
        if canti1 = 0 then
			set resultado = generar_email(p_nomb,p_apell,dominio);
            set editemail = true;
		end if;
        
		if NOT editemail then
			select count(*) into canti1 from empleado where id=idd and pais = paiss;
			if canti1 = 0 then
				set resultado = generar_email(p_nomb,p_apell,dominio);
			end if;
		end if;
        
        if NOT editemail then
			select count(*) into canti1 from empleado where id=idd and p_apellido = p_apell;
			if canti1 = 0 then
				set resultado = generar_email(p_nomb,p_apell,dominio);
                 set editemail = true;
			end if;
		end if;
        
		set f_registro = now();
		update empleado set p_apellido = p_apell, s_apellido = s_apell, p_nombre = p_nomb, s_nombre = s_nomb, t_documento = t_document,
			n_documento = n_document, pais = paiss, area = area1, f_ingreso = f_ingres,f_hora_registro = f_registro, email = resultado where id=idd;
		select "Actualizacion realizada";
END$$

DROP PROCEDURE IF EXISTS `guardar_empleado`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `guardar_empleado` (IN `p_apell` VARCHAR(20), IN `s_apell` VARCHAR(20), IN `p_nomb` VARCHAR(20), IN `s_nomb` VARCHAR(20), IN `t_document` VARCHAR(25), IN `n_document` VARCHAR(20), IN `f_ingres` VARCHAR(11), IN `area1` VARCHAR(25), IN `pais` VARCHAR(20))  BEGIN
	declare canti1 int;
    declare dominio varchar(20);
    declare f_registro varchar(20);
    declare resultado varchar(50);
    
	select count(*) into canti1 from empleado where n_documento = n_document and t_documento = t_document;
    
    if canti1 = 0 then
		if pais = 'Colombia' then
			set dominio = 'cidenet.com.co';
		else
			set dominio = 'cidenet.com.us';
		end if;
        
			set resultado = generar_email(p_nomb,p_apell,dominio);
        
			set f_registro = now();
			insert into empleado (p_apellido,s_apellido, p_nombre,s_nombre,t_documento,n_documento,pais,area,f_ingreso,f_hora_registro,email) 
				values (p_apell,s_apell,p_nomb,s_nomb,t_document,n_document,pais,area1,f_ingres,f_registro,resultado);
			select resultado;
	else
		set resultado = "id";
		select resultado;
    end if;
END$$

--
-- Funciones
--
DROP FUNCTION IF EXISTS `generar_email`$$
CREATE DEFINER=`root`@`localhost` FUNCTION `generar_email` (`p_nomb` VARCHAR(20), `p_apell` VARCHAR(20), `dominio` VARCHAR(20)) RETURNS VARCHAR(50) CHARSET utf8mb4 BEGIN
	declare canti1 int;
	declare cont int;
    declare resultado varchar(50);
    
	set cont = 1;
    set canti1 = 1;
		while canti1 = 1 do
			set resultado = concat(p_nomb,'.',p_apell,'.',cont,'@',dominio);
            select count(*) into canti1 from empleado where email = resultado;
            
            if canti1 = 0 then
				return resultado;
            end if;
			set cont = cont + 1;
        end while;
RETURN 1;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empleado`
--

DROP TABLE IF EXISTS `empleado`;
CREATE TABLE `empleado` (
  `id` int(11) NOT NULL,
  `p_apellido` varchar(20) DEFAULT NULL,
  `s_apellido` varchar(20) DEFAULT NULL,
  `p_nombre` varchar(20) DEFAULT NULL,
  `s_nombre` varchar(50) DEFAULT NULL,
  `t_documento` varchar(25) DEFAULT NULL,
  `n_documento` varchar(20) DEFAULT NULL,
  `pais` varchar(25) DEFAULT NULL,
  `area` varchar(25) DEFAULT NULL,
  `f_ingreso` varchar(11) DEFAULT NULL,
  `estado` varchar(10) DEFAULT 'Activo',
  `f_hora_registro` varchar(25) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `empleado`
--
ALTER TABLE `empleado`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `empleado`
--
ALTER TABLE `empleado`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
